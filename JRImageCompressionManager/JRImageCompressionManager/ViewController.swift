//
//  ViewController.swift
//  JRImageCompressionManager
//
//  Created by 京睿 on 2017/5/12.
//  Copyright © 2017年 JingRuiWangKe. All rights reserved.
//

import UIKit
import JRImagePicker

class ViewController: UIViewController {

    @IBOutlet weak var origin: UIImageView!
    @IBOutlet weak var result: UIImageView!
    @IBOutlet weak var logView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func selectImage(_ sender: UIButton) {
        let pk = JRImagePickerController.init { [weak self] (pk, image, info) in
            self?.log("origin \n - size: \(image!.size) \n - memo: \(image!.jMemsize)Kb")
            self?.origin.image = image
            pk.dismiss(animated: true, completion: nil)
        }
        present(pk, animated: true, completion: nil)
    }
    
    @IBAction func start(_ sender: UIButton) {
        guard let image = origin.image else {
            log("先“选择照片”")
            return
        }
        log("开始压缩……")
        JRImageCompressionManager.JRCompress(image, expect: 200) { [weak self] (result, _) in
            self?.log("压缩完毕……")
            self?.log("result \n - size: \(result!.size) \n - memo: \(result!.jMemsize)Kb")
            self?.result.image = result
        }
    }
    
    private func log(_ value: String) {
        logView.text = value + "\n*\(Int(NSDate().timeIntervalSince1970))*\n" + logView.text
    }
}

