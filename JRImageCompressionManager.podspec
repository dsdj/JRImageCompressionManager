#
#  Be sure to run `pod spec lint JRTest.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

s.name             = "JRImageCompressionManager"
s.version          = "1.0.3"
s.summary          = "JRImageCompressionManager图片压缩"

s.description      = <<-DESC
JRImageCompressionManager图片压缩
JRImageCompressionManager图片压缩
        DESC

s.homepage         = "http://git.oschina.net/dsdj/JRImageCompressionManager"

s.license          = 'MIT'
s.author           = { "NirvanAcN" => "mahaomeng@gmail.com" }
s.source           = { :git => "https://git.oschina.net/dsdj/JRImageCompressionManager.git", :tag => s.version.to_s }
s.social_media_url = 'http://weibo.com/2743943525'

s.ios.deployment_target = '8.0'
s.platform      = :ios, '8.0'
s.source_files  = 'JRImageCompressionManager/Needed/**/*'

s.frameworks = 'UIKit'

end
